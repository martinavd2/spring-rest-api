**Bitbucket checkout projects**

When you're done, please read README.md for setting up and running this application.

git clone https://martinavd2@bitbucket.org/martinavd2/spring-rest-api.git

---

## Setting up IDE

1. Install **Visual Studio Code**
2. Install extension **Spring Boot Extension Pack**
3. Install extension **SLombok Annotations Support for VS Code**

---

## Database dependency

1. You need PostgreSQL database on local
2. Database should contain a table "person" With columns personid, firstname, lastname

               CREATE TABLE public.person
                (
                    personid uuid NOT NULL DEFAULT gen_random_uuid(),
                    firstname character varying(100) COLLATE pg_catalog."default",
                    lastname character varying(100) COLLATE pg_catalog."default",
                    CONSTRAINT person_pkey PRIMARY KEY (personid)
                )
3. To default UUID for primary key CREATE EXTENSION pgcrypto
4. Set your postgres password in  /src/main/resources/application.properties

## Run Spring boot application

Now, you are all set for running spring-rest-api application.

1. On Left panel select **SPRING-BOOT-DASHBORD**
2. Right click on restapi and select debug to run in debug mode
3. On browser http://localhost:3002

----
