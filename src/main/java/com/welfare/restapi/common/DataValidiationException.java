package com.welfare.restapi.common;

public class DataValidiationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public DataValidiationException(String exception) {
      super(exception);
    }
    
  
  }