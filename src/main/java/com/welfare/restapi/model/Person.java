package com.welfare.restapi.model;

import java.util.UUID;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import lombok.*;

@Entity
@Table
@Data
public class Person {

    @Id
    @GeneratedValue
    @Column(name = "personid", unique = true, nullable = false, columnDefinition = "Person Identifier as UUID")
    private UUID personid;

    @NotBlank
    @Column(name = "firstname", columnDefinition = "Person first name as Text, allowed 100 characters")
    private String firstname;

    @NotBlank
    @Column(name = "lastname", columnDefinition = "Person last name as Text, allowed 100 characters")
    private String lastname;

}