package com.welfare.restapi.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties(prefix = "endpoints")
public class EndpointsProperties {


    private  String rulesEngine;

    public  String getRulesEngine(){
        return this.rulesEngine;
    }


    public  void setRulesEngine(String value){
        this.rulesEngine = value;
    }



   

}