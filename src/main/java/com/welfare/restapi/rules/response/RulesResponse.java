
package com.welfare.restapi.rules.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RulesResponse {

    @SerializedName("Case")
    @Expose
    public CaseObject _case;

    /**
     * No args constructor for use in serialization
     * 
     */
    public RulesResponse() {
    }

    /**
     * 
     * @param _case
     */
    public RulesResponse(CaseObject _case) {
        super();
        this._case = _case;
    }

}
