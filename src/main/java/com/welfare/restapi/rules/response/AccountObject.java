
package com.welfare.restapi.rules.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AccountObject {

    @SerializedName("Account_Id")
    @Expose
    public String accountId;
    @SerializedName("Amt_Soa")
    @Expose
    public Integer amtSoa;
    @SerializedName("Amt_Soa_Recent")
    @Expose
    public Integer amtSoaRecent;
    @SerializedName("Date_Effective")
    @Expose
    public String dateEffective;
    @SerializedName("Date_Next_Due")
    @Expose
    public String dateNextDue;
    @SerializedName("Frequency")
    @Expose
    public String frequency;
    @SerializedName("Payment")
    @Expose
    public List<PaymentObject> payment = null;
    @SerializedName("Amt_Balance_Rem")
    @Expose
    public Integer amtBalanceRem;
    @SerializedName("Type_Companion")
    @Expose
    public String typeCompanion;

    /**
     * No args constructor for use in serialization
     * 
     */
    public AccountObject() {
    }

    /**
     * 
     * @param dateNextDue
     * @param accountId
     * @param amtSoa
     * @param dateEffective
     * @param amtSoaRecent
     * @param payment
     * @param typeCompanion
     * @param frequency
     * @param amtBalanceRem
     */
    public AccountObject(String accountId, Integer amtSoa, Integer amtSoaRecent, String dateEffective, String dateNextDue, String frequency, List<PaymentObject> payment, Integer amtBalanceRem, String typeCompanion) {
        super();
        this.accountId = accountId;
        this.amtSoa = amtSoa;
        this.amtSoaRecent = amtSoaRecent;
        this.dateEffective = dateEffective;
        this.dateNextDue = dateNextDue;
        this.frequency = frequency;
        this.payment = payment;
        this.amtBalanceRem = amtBalanceRem;
        this.typeCompanion = typeCompanion;
    }

}
