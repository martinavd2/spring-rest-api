
package com.welfare.restapi.rules.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentObject {

    @SerializedName("Date_Due")
    @Expose
    public String dateDue;
    @SerializedName("Amount")
    @Expose
    public Double amount;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PaymentObject() {
    }

    /**
     * 
     * @param dateDue
     * @param amount
     */
    public PaymentObject(String dateDue, Double amount) {
        super();
        this.dateDue = dateDue;
        this.amount = amount;
    }

}
