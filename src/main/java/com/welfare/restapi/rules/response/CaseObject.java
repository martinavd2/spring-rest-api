
package com.welfare.restapi.rules.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CaseObject {

    @SerializedName("Account")
    @Expose
    public List<AccountObject> account = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CaseObject() {
    }

    /**
     * 
     * @param account
     */
    public CaseObject(List<AccountObject> account) {
        super();
        this.account = account;
    }

}
