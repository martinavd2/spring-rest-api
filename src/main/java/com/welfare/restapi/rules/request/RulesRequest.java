package com.welfare.restapi.rules.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "RuleApp",
    "RuleEngineServiceOptions",
    "RuleEngineServiceOutputTypes",
    "EntityName",
    "EntityState"
})
public class RulesRequest {

    @JsonProperty("RuleApp")
    public RuleApp ruleApp;
    @JsonProperty("RuleEngineServiceOptions")
    public RuleEngineServiceOptions ruleEngineServiceOptions;
    @JsonProperty("RuleEngineServiceOutputTypes")
    public RuleEngineServiceOutputTypes ruleEngineServiceOutputTypes;
    @JsonProperty("EntityName")
    public String entityName;
    @JsonProperty("EntityState")
    public String entityState;

}
