
package com.welfare.restapi.rules.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ActiveNotifications",
    "ActiveValidations",
    "EntityState",
    "Overrides",
    "RuleExecutionLog"
})
public class RuleEngineServiceOutputTypes {

    @JsonProperty("ActiveNotifications")
    public String activeNotifications;
    @JsonProperty("ActiveValidations")
    public String activeValidations;
    @JsonProperty("EntityState")
    public String entityState;
    @JsonProperty("Overrides")
    public String overrides;
    @JsonProperty("RuleExecutionLog")
    public String ruleExecutionLog;

}
