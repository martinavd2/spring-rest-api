package com.welfare.restapi.rules.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "RepositoryRuleAppRevisionSpec",
    "RepositoryServiceUri",
    "UseIntegratedSecurity",
    "UserName",
    "Password"
})
public class RuleApp {

    @JsonProperty("RepositoryRuleAppRevisionSpec")
    public RepositoryRuleAppRevisionSpec repositoryRuleAppRevisionSpec;
    @JsonProperty("RepositoryServiceUri")
    public String repositoryServiceUri;
    @JsonProperty("UseIntegratedSecurity")
    public String useIntegratedSecurity;
    @JsonProperty("UserName")
    public String userName;
    @JsonProperty("Password")
    public String password;

}
