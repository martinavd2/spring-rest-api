
package com.welfare.restapi.rules.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Date_Due",
    "Amount"
})
public class Payment {

    @JsonProperty("Date_Due")
    public String dateDue;
    @JsonProperty("Amount")
    public Double amount;

}
