
package com.welfare.restapi.rules.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Account_Id",
    "Amt_Soa",
    "Date_Effective",
    "Date_Next_Due",
    "Frequency"
})
public class Account {

    @JsonProperty("Account_Id")
    public String accountId;
    @JsonProperty("Amt_Soa")
    public String amtSoa;
    @JsonProperty("Date_Effective")
    public String dateEffective;
    @JsonProperty("Date_Next_Due")
    public String dateNextDue;
    @JsonProperty("Frequency")
    public String frequency;

}
