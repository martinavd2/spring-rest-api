package com.welfare.restapi.rules.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "RuleApplicationName"
})
public class RepositoryRuleAppRevisionSpec {

    @JsonProperty("RuleApplicationName")
    public String ruleApplicationName;

}
