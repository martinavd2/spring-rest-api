package com.welfare.restapi.response;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;


public class PersonSearchRes {

    @JsonProperty("PersonId")
    private String personId;

    @JsonProperty("Firstname")
    private String firstName;

    @JsonProperty("Status")
    private String Status;

    public PersonSearchRes() {}

    public static List<PersonSearchRes> fromEntity (final List<Map<String, String>> p) {

        List<PersonSearchRes> res = new ArrayList<PersonSearchRes>();

        for (Map<String, String> map : p) {

            PersonSearchRes r = new PersonSearchRes();

            r.personId    = map.get("personid").toString();
            r.firstName   = map.get("firstname").toString();
            r.Status      = map.get("status").toString();

            res.add(r);

        }

        return res;
    }

}