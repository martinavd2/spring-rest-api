package com.welfare.restapi.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import com.welfare.restapi.common.DataValidiationException;
import com.welfare.restapi.model.Person;
import com.welfare.restapi.repository.PersonRepository;
import com.welfare.restapi.response.PersonSearchRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin
@Api(value="Person", description="<small>Operations pertaining to Person---==</small>", tags = { "Person" })
public class PersonController  {

    @Autowired
    private PersonRepository personRepository;

    @GetMapping("/persons")
    public List<Person> GetAllPersons() {

        return personRepository.findAll();

    }

    @PostMapping("/persons")
    public Person SavePerson(@Valid @RequestBody final Person person) {

        return personRepository.save(person);

    }

    @GetMapping("/persons/{id}")
    public Optional<Person> GetPerson(@PathVariable final UUID id) {

        return personRepository.findById(id); 

    }


    @GetMapping("/persons/{firstname}/{lastname}")
    public List<Person> GetPerson(@PathVariable final String firstname, @PathVariable final String lastname) {

        return personRepository.findByName(firstname, lastname);

    }


    @GetMapping("/persons/search/{firstname}")
    public ResponseEntity<List<PersonSearchRes>> GetPersonByName(@PathVariable final String firstname) {


        final List<Map<String, String>> p = personRepository.findByPersonName(firstname);
        final List<PersonSearchRes> res = (List<PersonSearchRes>) PersonSearchRes.fromEntity(p);

        if (p.size() == 0) {
            throw new DataValidiationException("Data not valid!!");
        }
        
        return  new ResponseEntity<>(res, HttpStatus.OK);

    }



    @ExceptionHandler(DataValidiationException.class)
    public ResponseEntity<Object> handleException(DataValidiationException  e) {
        return  new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }



}