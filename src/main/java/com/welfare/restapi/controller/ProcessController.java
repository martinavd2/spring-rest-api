package com.welfare.restapi.controller;

import java.util.ArrayList;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.welfare.restapi.config.EndpointsProperties;
import com.welfare.restapi.rules.request.Account;
import com.welfare.restapi.rules.request.AccrualChargeRequest;
import com.welfare.restapi.rules.request.Case;
import com.welfare.restapi.rules.request.RepositoryRuleAppRevisionSpec;
import com.welfare.restapi.rules.request.RuleApp;
import com.welfare.restapi.rules.request.RulesRequest;
import com.welfare.restapi.rules.response.RulesResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
@CrossOrigin
public class ProcessController {

    @Value("${endpoints.ruleEngine}")
    private String endpoint;

    @GetMapping("/process/generatecharges")
    public String GenerateAccrualCharges() throws JsonProcessingException {

        final RulesRequest req = prepare_data_for_RulesRequest();

        ObjectMapper objectMapper = new ObjectMapper();
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntityStr = restTemplate.postForEntity(this.endpoint, req, String.class);
        JsonNode root = objectMapper.readTree(responseEntityStr.getBody());

        Gson g = new Gson(); 
        String payload = root.get("EntityState").toPrettyString().replaceAll("\\\\", "").replaceAll("^\"|\"$", "");
        RulesResponse resp = g.fromJson(payload, RulesResponse.class);

        return resp._case.account.get(0).dateNextDue;

    }

    private RulesRequest prepare_data_for_RulesRequest() throws JsonProcessingException {

        final RulesRequest req = new RulesRequest();
        final AccrualChargeRequest accuralCharge = new AccrualChargeRequest();

        req.ruleApp = new RuleApp();
        req.ruleApp.repositoryRuleAppRevisionSpec = new RepositoryRuleAppRevisionSpec();

        req.ruleApp.repositoryRuleAppRevisionSpec.ruleApplicationName = "Charging";
        req.ruleApp.repositoryServiceUri = "http://EC2AMAZ-0HA6HT6/InRuleCatalogService/service.svc";
        req.ruleApp.useIntegratedSecurity = "false";
        req.ruleApp.userName = "Admin";
        req.ruleApp.password = "password";

        accuralCharge._case = new Case();
        accuralCharge._case.account = new ArrayList<Account>();

        final Account acct = new Account();
        acct.accountId = "11111";
        acct.amtSoa = "400";
        acct.dateEffective = "2020-03-25";
        acct.dateNextDue = "2020-03-25";
        acct.frequency = "bi-weekly";

        accuralCharge._case.account.add(acct);

        final ObjectMapper mapper = new ObjectMapper();
        final String sAccuralCharge = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(accuralCharge);
        req.entityState =  sAccuralCharge;
        req.entityName = "Payor";

        return req;

    }

}