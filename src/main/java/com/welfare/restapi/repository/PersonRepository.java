package com.welfare.restapi.repository;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import com.welfare.restapi.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface PersonRepository extends JpaRepository<Person, UUID> {

    @Query(value = "SELECT * FROM person p WHERE p.firstname=?1 AND p.lastname =?2", nativeQuery =  true)
     public List<Person> findByName(final String firstname, final String lastname); 
    //public List<Person> findByFirstName(@Param("firstname") String firstname); 


     @Query(value = "SELECT * FROM person p WHERE p.personid =?1", nativeQuery =  true)
     public Person findByPersonId(final UUID personid);


     @Query(value = "SELECT * FROM getpersonbyname(?1)", nativeQuery =  true)
     public List<Map<String, String>> findByPersonName(final String firstname);


}